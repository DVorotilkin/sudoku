import collections
import copy


class SudokuSolver:

    class LastValue(Exception): pass

    class NoValue(Exception): pass

    class TrySet:
        def __init__(self, sudoku, indices):
            self.sudoku = sudoku
            self.indices = indices

    def __init__(self, input_table):
        def filler(number):
            match number:
                case 0:
                    return [i for i in range(1, 10)]
                case n:
                    return [n]

        self.sudoku = []
        for row in input_table:
            res_row = []
            for value in row:
                res_row.append(filler(value))
            self.sudoku.append(res_row)
        self.last_tries_sudoku = []

    @staticmethod
    def read_file(file_name):
        input_table = []
        with open(file_name, 'r') as f:
            for line in f:
                def parser(c):
                    match c:
                        case '\n':
                            return 0
                        case '':
                            return 0
                        case cc:
                            return int(cc)

                # print([c for c in line.split('\t')])
                numbers = list(map(parser, line.split('\t')))
                prefix = [0] * max(0, 9 - len(numbers))
                numbers = prefix + numbers
                input_table.append(numbers)
                # print(numbers, len(numbers))
        print(*input_table, sep="\n")
        return input_table

    def print_sudoku(self, tag=''):
        print('='*100 + tag)
        for row_i, row in enumerate(self.sudoku):
            max_val_count = max(map(len, row))
            for val_i in range(0, max_val_count):
                for values in row:
                    if len(values) <= val_i:
                        print('   ', end='')
                    else:
                        print(f'{values[val_i]}, ', end='')
                if val_i == 0:
                    print('-'*10 + str(row_i))
                else:
                    print('')

    def __rm_extra_values(self, ir, ic, v):
        square_row_index = ir // 3
        square_column_index = ic // 3

        def rm_val(val):
            if len(val) != 1:
                return False
            removed = False
            try:
                v.remove(val[0])
                removed = True
            except ValueError:
                removed = False
            if len(v) == 1:
                raise SudokuSolver.LastValue
            return removed

        try:
            res = False
            for row_i, row_val in filter(lambda x: x[0] != ic, enumerate(self.sudoku[ir])):
                res = rm_val(row_val) or res

            for col_i, row in filter(lambda x: x[0] != ir, enumerate(self.sudoku)):
                res = rm_val(row[ic]) or res

            for iir in range(square_row_index * 3, square_row_index * 3 + 3):
                for iic in range(square_column_index * 3, square_column_index * 3 + 3):
                    if iir == ir and iic == ic:
                        continue
                    res = rm_val(self.sudoku[iir][iic]) or res
        except SudokuSolver.LastValue:
            return True
        return res

    def __set_unique_values(self, ir, ic, v):
        square_row_index = ir // 3
        square_column_index = ic // 3
        vv = []

        def rm_duplicated_val(val):
            for v_val in val:
                try:
                    vv.remove(v_val)
                except ValueError:
                    pass

        vv = v.copy()
        try:
            for _, row_val in filter(lambda x: x[0] != ic, enumerate(self.sudoku[ir])):
                rm_duplicated_val(row_val)
            if len(vv) == 1:
                raise SudokuSolver.LastValue
            vv = v.copy()
            for _, row in filter(lambda x: x[0] != ir, enumerate(self.sudoku)):
                rm_duplicated_val(row[ic])
            if len(vv) == 1:
                raise SudokuSolver.LastValue
            vv = v.copy()
            for iir in range(square_row_index * 3, square_row_index * 3 + 3):
                for iic in range(square_column_index * 3, square_column_index * 3 + 3):
                    if iir == ir and iic == ic:
                        continue
                    rm_duplicated_val(self.sudoku[iir][iic])
            if len(vv) == 1:
                raise SudokuSolver.LastValue
        except SudokuSolver.LastValue:
            self.sudoku[ir][ic] = vv
            return True
        return False

    def __find_next_item(self, start_row=0, start_col=0):

        for ir in range(start_row, 9):
            for ic in range(start_col, 9):
                if len(self.sudoku[ir][ic]) > 1:
                    return ir, ic, 0
        raise SudokuSolver.NoValue

    def __next_try(self, with_backup):
        print("Random helps!)" + str(with_backup), end='')
        last_try_set = SudokuSolver.TrySet([], (0, 0, 0))
        while True:
            if with_backup:
                while self.last_tries_sudoku:
                    last_try_set = self.last_tries_sudoku[-1]
                    self.sudoku = copy.deepcopy(last_try_set.sudoku)
                    row, col, val_i = last_try_set.indices
                    val = self.sudoku[row][col]
                    if len(val) - 1 <= val_i:
                        try:
                            row, col, val_i = self.__find_next_item(row, col + 1)
                        except SudokuSolver.NoValue:
                            del self.last_tries_sudoku[-1]
                            continue
                    else:
                        val_i = val_i + 1
                    self.last_tries_sudoku[-1].indices = (row, col, val_i)
                    break
                else:
                    raise SudokuSolver.NoValue
                break
            else:
                try:
                    last_try_set = SudokuSolver.TrySet(copy.deepcopy(self.sudoku), self.__find_next_item())
                    self.last_tries_sudoku.append(last_try_set)
                    break
                except SudokuSolver.NoValue:
                    with_backup = True
                    continue
        row, col, val_i = last_try_set.indices
        val = self.sudoku[row][col]
        self.sudoku[row][col] = [val[val_i]]
        print(last_try_set.indices)

    def __check(self):
        for row_i, row in enumerate(self.sudoku):
            c = collections.defaultdict(bool)
            for col_i, values in enumerate(row):
                if len(values) == 1:
                    if values[0] in c:
                        self.print_sudoku()
                        print(f'Fail at {row_i}, {col_i}')
                        return False
                    c[values[0]] = True
        for col_i in range(0, 9):
            c = collections.defaultdict(bool)
            for row_i, row in enumerate(self.sudoku):
                values = row[col_i]
                if len(values) == 1:
                    if values[0] in c:
                        self.print_sudoku()
                        print(f'Fail at {row_i}, {col_i}')
                        return False
                    c[values[0]] = True
        return True

    def resolve(self):
        iteration_count = 0
        while status:
            status = False
            removed = False
            for ir, r in enumerate(self.sudoku):
                for ic, v in enumerate(r):
                    if len(v) > 1:
                        status = True
                        removed = self.__rm_extra_values(ir, ic, v) or removed
            if not removed:
                self.print_sudoku(str(iteration_count) + "Before unique")
                for ir, r in enumerate(self.sudoku):
                    for ic, v in enumerate(r):
                        if len(v) > 1:
                            status = True
                            removed = self.__set_unique_values(ir, ic, v) or removed

            if not self.__check():
                status = True
                self.__next_try(True)
            if not removed and status:
                self.print_sudoku(str(iteration_count) + "Before Rand")
                self.__next_try(False)
            self.print_sudoku(str(iteration_count))
            if self.__check() and not status:
                break
            iteration_count += 1
        print(self.__check())


if __name__ == '__main__':
    solver = SudokuSolver(SudokuSolver.read_file('sudoku.txt'))
    solver.resolve()




